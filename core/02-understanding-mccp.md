---
title: Understanding MCCP
---

## MCCP Design Pattern

Model-CollectionService-Controller-Presenter (MCCP) Design Pattern is a variant of Model-View-Controller object-oriented design pattern where Model and Data Service objects are two different entities in its processing sequence. Data structure is represented by a Model. Data processing service is handled by CollectionService.

Refer to the following diagram of how a request is processed with the MCCP design pattern.

![](/img/core/MCCP.svg)

The flow of the request processing is as follows:
1. When a request is made from a web browser (from typing an URL on a browser address bar or by a Javascript AJAX request.), the request is received by a Controller. 
2. The Controller decides what to do with the request. If the request requires some data from the database, the Controller should request the data from the CollectionService. 
3. The CollectionService collects/processes the data from the database and returns the requested data back to the Controller. 
4. Later, the Controller should return the data back to the Web browser of the correct requested format (HTML or JSON document) by loading a template file to present the data from the `app/view` directory. 
5. The presented data with the template will then be returned to the Web browser.

Frontend processing is executed in the Client web browser, while backend processing is executed by the web server and database server. Therefore, a full Apache-PHP-MySQL stack is required for development of a web application with Core Framework.

In Windows, the Apache-PHP-MySQL stack can be installed with an XAMPP installation package which can be downloaded from:

https://www.apachefriends.org/index.html

On Linux/macOS the Apache-PHP-MySQL stack can be installed by installing the module separately. On a fresh install of macOS, the Apache web server with PHP module is already installed, but it is not enabled by default. However, macOS users need to install MySQL server on their system.

### Controller

A Controller is the main entry point for an application. It serves the logical processing flow of the application. A Controller is responsible to accept a HTTP request and returns the requested data/information in a particular format. 

The minimal boilerplate for an application should consist of a Controller. When a HTTP request is received by a controller, it is up to the Controller to decide the appropriate data processing flow for the request. Even tough all the responsibility of Model, CollectionService, and View can be performed by only the Controller, it is not recommended to do so.

[More information about Controller](getting-started/controller).

### Model

A Model represents an operable tangible/intangible entity of the application. In most cases, a Model represent the data processed by the application. A model can represent a single entity or object or represent a representation of several related entities/objects. For example, a Model of Car could represent a single entity of car, but also represent a group representation of parts of a car, such as Window, Wheel, Body, Driver, etc.

Depending on the complexity level of the application, the use of a Model in an application is purely optional.

[More information about Model](getting-started/model).

### CollectionService

**CollectionService** or a **Service** is the data service processing entity for Model. Data communication between web application and the database server is handled by CollectionService.

[More information about CollectionService](getting-started/collection-service).

### View and Presenter

In Core Framework, **View** is not supposed to be extended as a type of an application View entity. View is a special helper entity used by an application Controller to present/visualize data obtained from CollectionService into various format. HTML templates are used to present the data and are located in the `app/view` directory.

A View is presented by the Controller from a View object that reside inside the Controller. However, a presented View template inherits the View object, so the template can also present another View template for reuse.

[More information about View](getting-started/view).


<!-- Core Framework is a PHP-based web application framework with a small footprint with exceptional performance. It adapts the Model-View-Controller design pattern into Model-CollectionService-Controller-Presenter (MCCP) design pattern to turn a linear web application processing flow into object-oriented programming style. -->