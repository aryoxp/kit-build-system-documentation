---
# id: halo
# slug: halo
title: Global API
sidebar_position: 1
# author: Yangshun Tay
# author_title: Front End Engineer @ Facebook
# author_url: https://github.com/yangshun
# author_image_url: https://avatars0.githubusercontent.com/u/1315101?s=400&v=4
# tags: [facebook, hello, docusaurus]
---

## Global Variables Definition

Path to various framework location. Path refer to the file system location of an item, and not an URL.
All variables includes a trailing slash. 

- `CORE` : Path to the directory where index.php is located.
- `CORE_LIBRARY` : Path to Core-Framework library directory.
- `CORE_CVS` : Path to the main Controller-View-Service base class.
- `CORE_CONTROLLER` : Path to the application controller directory.
- `CORE_SERVICE` : Path to the application collection service directory.
- `CORE_MODEL` : Path to the application model directory.
- `CORE_VIEW` : Path to the application view template directory.
- `CORE_APPLIBRARY` : Path to the application custom library directory.

***Deprecated***

- `CORE_BASE_PATH` : Path to the application current working directory.
- `CORE_APP_PATH` : Path to the application directory.

