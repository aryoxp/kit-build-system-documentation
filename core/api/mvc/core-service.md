---
title: CoreService
---

#### Description

CoreService description.

## Methods

### getInstance

#### Prototype

```php
protected getInstance (key)
```

#### Parameters

#### Return Value

---

### instance

#### Prototype

```php
private static instance (configKey)
```

#### Parameters

#### Return Value

---

### getConfig

#### Prototype

```php
protected static getConfig (configKey)
```

#### Parameters

#### Return Value

---

