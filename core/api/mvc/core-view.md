---
title: CoreView
---

#### Description

CoreView description.

## Methods

### __construct

#### Prototype

```php
private __construct (controller)
```

#### Parameters

#### Return Value

---

### instance

#### Prototype

```php
public static instance (controller)
```

#### Parameters

#### Return Value

---

### language

#### Prototype

```php
public language (path)
```

#### Parameters

#### Return Value

---

### l

#### Prototype

```php
public l (key, params)
```

#### Parameters

#### Return Value

---

### json

#### Prototype

```php
public json (data)
```

#### Parameters

#### Return Value

---

### view

#### Prototype

```php
public view (view, data, return)
```

#### Parameters

#### Return Value

---

### location

#### Prototype

```php
public location (path, secure)
```

#### Parameters

#### Return Value

---

### assets

#### Prototype

```php
public assets (path, secure)
```

#### Parameters

#### Return Value

---

### addStyle

#### Prototype

```php
public addStyle (path, opt)
```

#### Parameters

#### Return Value

---

### addScript

#### Prototype

```php
public addScript (opath, opt)
```

#### Parameters

#### Return Value

---

### addPlugin

#### Prototype

```php
public addPlugin (key, opt)
```

#### Parameters

#### Return Value

---

