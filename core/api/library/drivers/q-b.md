---
title: QB
---

#### Description

QB description.

## Methods

### __construct

#### Prototype

```php
public __construct (table, dbConfigKeyOrDb)
```

#### Parameters

#### Return Value

---

### setKey

#### Prototype

```php
public setKey (dbConfigKeyOrDb)
```

#### Parameters

#### Return Value

---

### setData

#### Prototype

```php
public setData (_args)
```

#### Parameters

#### Return Value

---

### getData

#### Prototype

```php
public getData ()
```

#### Parameters

#### Return Value

---

### table

#### Prototype

```php
public table (table)
```

#### Parameters

#### Return Value

---

### get

#### Prototype

```php
public get ()
```

#### Parameters

#### Return Value

---

### begin

#### Prototype

```php
public begin ()
```

#### Parameters

#### Return Value

---

### commit

#### Prototype

```php
public commit ()
```

#### Parameters

#### Return Value

---

### rollback

#### Prototype

```php
public rollback ()
```

#### Parameters

#### Return Value

---

### execute

#### Prototype

```php
public execute ()
```

#### Parameters

#### Return Value

---

### executeQuery

#### Prototype

```php
public executeQuery (asObject)
```

#### Parameters

#### Return Value

---

### map

#### Prototype

```php
public map (modelOrClassName)
```

#### Parameters

#### Return Value

---

### insertId

#### Prototype

```php
public insertId ()
```

#### Parameters

#### Return Value

---

### result

#### Prototype

```php
public result ()
```

#### Parameters

#### Return Value

---

### getFields

#### Prototype

```php
public getFields ()
```

#### Parameters

#### Return Value

---

### callback

#### Prototype

```php
public callback (functionName, args)
```

#### Parameters

#### Return Value

---

### clear

#### Prototype

```php
public clear (clearType)
```

#### Parameters

#### Return Value

---

### raw

#### Prototype

```php
public static raw (string)
```

#### Parameters

#### Return Value

---

### instance

#### Prototype

```php
public static instance (dbConfigKeyOrDb, table)
```

#### Parameters

#### Return Value

---

### bt

#### Prototype

```php
public static bt (column)
```

#### Parameters

#### Return Value

---

### qt

#### Prototype

```php
public static qt (value)
```

#### Parameters

#### Return Value

---

### connector

#### Prototype

```php
public static connector (connectorType)
```

#### Parameters

#### Return Value

---

### esc

#### Prototype

```php
public static esc (value)
```

#### Parameters

#### Return Value

---

### fields

#### Prototype

```php
protected static fields (model)
```

#### Parameters

#### Return Value

---

### getInsertId

#### Prototype

```php
public getInsertId ()
```

#### Parameters

#### Return Value

---

### getAffectedRows

#### Prototype

```php
public getAffectedRows ()
```

#### Parameters

#### Return Value

---

### getInstance

#### Prototype

```php
protected getInstance (key)
```

#### Parameters

#### Return Value

---

### getConfig

#### Prototype

```php
protected static getConfig (configKey)
```

#### Parameters

#### Return Value

---

