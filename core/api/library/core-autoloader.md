---
title: CoreAutoloader
---

#### Description

CoreAutoloader description.

## Methods

### __construct

#### Prototype

```php
public __construct ()
```

#### Parameters

#### Return Value

---

### libraryLoader

#### Prototype

```php
private libraryLoader (className)
```

#### Parameters

#### Return Value

---

### driverLoader

#### Prototype

```php
private driverLoader (className)
```

#### Parameters

#### Return Value

---

### cvsLoader

#### Prototype

```php
private cvsLoader (className)
```

#### Parameters

#### Return Value

---

### appLibraryLoader

#### Prototype

```php
private appLibraryLoader (className)
```

#### Parameters

#### Return Value

---

### controllerLoader

#### Prototype

```php
private controllerLoader (className)
```

#### Parameters

#### Return Value

---

### serviceLoader

#### Prototype

```php
private serviceLoader (className)
```

#### Parameters

#### Return Value

---

### modelLoader

#### Prototype

```php
private modelLoader (className)
```

#### Parameters

#### Return Value

---

### register

#### Prototype

```php
public static register (loader)
```

#### Parameters

#### Return Value

---

