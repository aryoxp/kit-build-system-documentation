---
title: IDatabase
---

#### Description

IDatabase description.

## Methods

### connect

#### Prototype

```php
abstract public connect ()
```

#### Parameters

#### Return Value

---

### disconnect

#### Prototype

```php
abstract public disconnect ()
```

#### Parameters

#### Return Value

---

### getVersion

#### Prototype

```php
abstract public getVersion ()
```

#### Parameters

#### Return Value

---

### getInsertId

#### Prototype

```php
abstract public getInsertId ()
```

#### Parameters

#### Return Value

---

### getAffectedRows

#### Prototype

```php
abstract public getAffectedRows ()
```

#### Parameters

#### Return Value

---

### getError

#### Prototype

```php
abstract public getError ()
```

#### Parameters

#### Return Value

---

### query

#### Prototype

```php
abstract public query (query)
```

#### Parameters

#### Return Value

---

### getVar

#### Prototype

```php
abstract public getVar (query)
```

#### Parameters

#### Return Value

---

### getRow

#### Prototype

```php
abstract public getRow (query)
```

#### Parameters

#### Return Value

---

### begin

#### Prototype

```php
abstract public begin ()
```

#### Parameters

#### Return Value

---

### commit

#### Prototype

```php
abstract public commit ()
```

#### Parameters

#### Return Value

---

### rollback

#### Prototype

```php
abstract public rollback ()
```

#### Parameters

#### Return Value

---

### escape

#### Prototype

```php
abstract public escape (data)
```

#### Parameters

#### Return Value

---

