---
title: CoreMessage
---

#### Description

CoreMessage description.

## Methods

### __construct

#### Prototype

```php
private __construct (message, type)
```

#### Parameters

#### Return Value

---

### instance

#### Prototype

```php
public static instance (message, type)
```

#### Parameters

#### Return Value

---

### get

#### Prototype

```php
public get (kind)
```

#### Parameters

#### Return Value

---

