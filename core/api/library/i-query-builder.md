---
title: IQueryBuilder
---

#### Description

IQueryBuilder description.

## Methods

### table

#### Prototype

```php
abstract public table (table)
```

#### Parameters

#### Return Value

---

### select

#### Prototype

```php
abstract public select (columns)
```

#### Parameters

#### Return Value

---

### selectModel

#### Prototype

```php
abstract public selectModel (model)
```

#### Parameters

#### Return Value

---

### distinct

#### Prototype

```php
abstract public distinct ()
```

#### Parameters

#### Return Value

---

### insert

#### Prototype

```php
abstract public insert (columnValues, ignore)
```

#### Parameters

#### Return Value

---

### insertIgnore

#### Prototype

```php
abstract public insertIgnore (columnValues)
```

#### Parameters

#### Return Value

---

### insertModel

#### Prototype

```php
abstract public insertModel (models, fields)
```

#### Parameters

#### Return Value

---

### ignore

#### Prototype

```php
abstract public ignore ()
```

#### Parameters

#### Return Value

---

### delete

#### Prototype

```php
abstract public delete ()
```

#### Parameters

#### Return Value

---

### deleteModel

#### Prototype

```php
abstract public deleteModel (models)
```

#### Parameters

#### Return Value

---

### update

#### Prototype

```php
abstract public update (columnValues)
```

#### Parameters

#### Return Value

---

### updateModel

#### Prototype

```php
abstract public updateModel (model)
```

#### Parameters

#### Return Value

---

### orderBy

#### Prototype

```php
abstract public orderBy (column, order)
```

#### Parameters

#### Return Value

---

### where

#### Prototype

```php
abstract public where (column, opvalue, value)
```

#### Parameters

#### Return Value

---

### orWhere

#### Prototype

```php
abstract public orWhere (column, opvalue, value)
```

#### Parameters

#### Return Value

---

### whereIn

#### Prototype

```php
abstract public whereIn (column, values)
```

#### Parameters

#### Return Value

---

### whereNotIn

#### Prototype

```php
abstract public whereNotIn (column, values)
```

#### Parameters

#### Return Value

---

### whereNull

#### Prototype

```php
abstract public whereNull (column)
```

#### Parameters

#### Return Value

---

### whereNotNull

#### Prototype

```php
abstract public whereNotNull (column)
```

#### Parameters

#### Return Value

---

### whereBetween

#### Prototype

```php
abstract public whereBetween (column, min, max)
```

#### Parameters

#### Return Value

---

### whereNotBetween

#### Prototype

```php
abstract public whereNotBetween (column, min, max)
```

#### Parameters

#### Return Value

---

### whereColumn

#### Prototype

```php
abstract public whereColumn (columnLeft, opColumnRight, columnRight)
```

#### Parameters

#### Return Value

---

### whereGroup

#### Prototype

```php
abstract public whereGroup (qb)
```

#### Parameters

#### Return Value

---

### whereExists

#### Prototype

```php
abstract public whereExists (qb)
```

#### Parameters

#### Return Value

---

### groupBy

#### Prototype

```php
abstract public groupBy (columns)
```

#### Parameters

#### Return Value

---

### having

#### Prototype

```php
abstract public having (column, opValue, value)
```

#### Parameters

#### Return Value

---

### orHaving

#### Prototype

```php
abstract public orHaving (column, opValue, value)
```

#### Parameters

#### Return Value

---

### get

#### Prototype

```php
abstract public get ()
```

#### Parameters

#### Return Value

---

### max

#### Prototype

```php
abstract public max (column)
```

#### Parameters

#### Return Value

---

### count

#### Prototype

```php
abstract public count (column)
```

#### Parameters

#### Return Value

---

### queryRaw

#### Prototype

```php
abstract public queryRaw (sql, paramValues)
```

#### Parameters

#### Return Value

---

### selectRaw

#### Prototype

```php
abstract public selectRaw (sql, paramValues)
```

#### Parameters

#### Return Value

---

### whereRaw

#### Prototype

```php
abstract public whereRaw (sql, paramValues)
```

#### Parameters

#### Return Value

---

### orWhereRaw

#### Prototype

```php
abstract public orWhereRaw (sql, paramValues)
```

#### Parameters

#### Return Value

---

### havingRaw

#### Prototype

```php
abstract public havingRaw (sql, paramValues)
```

#### Parameters

#### Return Value

---

### orHavingRaw

#### Prototype

```php
abstract public orHavingRaw (sql, paramValues)
```

#### Parameters

#### Return Value

---

### join

#### Prototype

```php
abstract public join (table, leftColumn, operator, rightColumn)
```

#### Parameters

#### Return Value

---

### leftJoin

#### Prototype

```php
abstract public leftJoin (table, leftColumn, operator, rightColumn)
```

#### Parameters

#### Return Value

---

### crossJoin

#### Prototype

```php
abstract public crossJoin (table)
```

#### Parameters

#### Return Value

---

### limit

#### Prototype

```php
abstract public limit (offset, limit)
```

#### Parameters

#### Return Value

---

### page

#### Prototype

```php
abstract public page (page, perPage)
```

#### Parameters

#### Return Value

---

