---
title: CoreResult
---

#### Description

CoreResult description.

## Methods

### __construct

#### Prototype

```php
public __construct (result, status, error)
```

#### Parameters

#### Return Value

---

### show

#### Prototype

```php
public show ()
```

#### Parameters

#### Return Value

---

### instance

#### Prototype

```php
public static instance (result, status, error)
```

#### Parameters

#### Return Value

---

