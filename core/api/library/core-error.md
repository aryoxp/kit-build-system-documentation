---
title: CoreError
---

#### Description

CoreError description.

## Methods

### show

#### Prototype

```php
public show ()
```

#### Parameters

#### Return Value

---

### instance

#### Prototype

```php
public static instance (message)
```

#### Parameters

#### Return Value

---

### __clone

#### Prototype

```php
final private __clone ()
```

#### Parameters

#### Return Value

---

### __construct

#### Prototype

```php
public __construct (message, code, previous)
```

#### Parameters

#### Return Value

---

### __wakeup

#### Prototype

```php
public __wakeup ()
```

#### Parameters

#### Return Value

---

### getMessage

#### Prototype

```php
final public getMessage ()
```

#### Parameters

#### Return Value

---

### getCode

#### Prototype

```php
final public getCode ()
```

#### Parameters

#### Return Value

---

### getFile

#### Prototype

```php
final public getFile ()
```

#### Parameters

#### Return Value

---

### getLine

#### Prototype

```php
final public getLine ()
```

#### Parameters

#### Return Value

---

### getTrace

#### Prototype

```php
final public getTrace ()
```

#### Parameters

#### Return Value

---

### getPrevious

#### Prototype

```php
final public getPrevious ()
```

#### Parameters

#### Return Value

---

### getTraceAsString

#### Prototype

```php
final public getTraceAsString ()
```

#### Parameters

#### Return Value

---

### __toString

#### Prototype

```php
public __toString ()
```

#### Parameters

#### Return Value

---

