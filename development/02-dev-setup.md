---
title: "Development Setup"
---

Kit-Build Concept Map Installation Guide for Development on Windows Platform 

<!-- ### Windows 10 -->

## OVERVIEW

This guide applies to the development environment of the Kit-Build concept map system on Windows 10 PC.

## GOALS

1. Setting up the development environment platform
2. Clone the source code
3. Initialize data and configuring system

## REQUIREMENTS
- Windows 10 PC
- Google Chrome, Chromium Edge, or Mozilla Firefox

### Required Software Download
* XAMPP for Windows - Apache 2.4, PHP 7.x, MySQL 5.7
* Git Client (Sourcetree) for Windows


________________
## SETTING UP PREREQUISITES

### Download and Install Apache 2.4, PHP 7.x, and MySQL 5.7 with XAMPP Package

Use a web browser to  and open the following URL:
https://www.apachefriends.org/download.html

Download the latest release of version 7 (with PHP 7). 

![](/img/local-dev-install/image17.png)

:::caution

**Do NOT** download the version 8 release. 
Current Kit-Build system source code is not compatible with PHP 8 which is included in version 8 of XAMPP.

:::  

#### Checking Port Availability

Start "Command Prompt" and issue the following command:

`netstat -an | findstr "LISTEN"`

Check and verify lines that NO service or application uses port 80 (Apache) or 3306 (MySQL) are currently listening, like: 

`0.0.0.0:80`

Or 

`0.0.0.0:3306`

![](/img/local-dev-install/image36.png)
  
:::caution
If there is a line that says `0.0.0.0:80` from the list, it means port 80 is currently used by another application/service or probably Apache is already installed on the computer. If that is the case, then you should install Apache service on another port other than port 80.
:::

#### Installing Apache 2.4, PHP 7.x, and MySQL 5.7 with XAMPP Package

Check your Download folder to find the downloaded XAMPP package and start the installer.

Click OK to the following warning message when starting the installer. XAMPP cannot be installed on the `C:\Program Files` directory because of write access restriction. Thus we will install XAMPP in the `C:\xampp directory.`

![](/img/local-dev-install/image20.png)
  
Click Next on the initial setup screen.

![](/img/local-dev-install/image39.png)
  
Select only the following components from the Select Components screen:
   * Apache
   * MySQL (MariaDB 10 will be installed instead of MySQL)
   * PHP
   * phpMyAdmin

![](/img/local-dev-install/image26.png)

:::note
The included database server from XAMPP is MariaDB, not MySQL. MariaDB was a branch clone from the open source version of MySQL 5. In rare situations, using MariaDB instead of MySQL may cause compatibility problems with the online server configuration (which uses MySQL for the database server), unless you will use specific functions of the newer MariaDB or MySQL. 

However, for the current development of the KB system, it is OK to use MariaDB as both servers have the same behaviors. 

Nevertheless, you can install the MySQL database server which can be installed using a separate MySQL installer. 

:::

Click Next, and set the installation folder to `C:\xampp`

![](/img/local-dev-install/image12.png)
  
Click the Next button, and set the installation language to English

![](/img/local-dev-install/image33.png)

Click Next, and uncheck the checkbox to skip the "Learn more about Bitnami for XAMPP..."

![](/img/local-dev-install/image14.png)
  
Click Next twice to start the installation process.

![](/img/local-dev-install/image41.png)
  
If during installation process the following Security Alert dialogs appear, check (tick) the Private networks checkbox and click Allow access button.

![](/img/local-dev-install/image25.png)

![](/img/local-dev-install/image2.png)

![](/img/local-dev-install/image10.png)
  
XAMPP Control Panel can be shown or hidden by clicking the XAMPP icon from the "Notification Area" or "System Tray".

![](/img/local-dev-install/image8.png)
  
On the XAMPP Control Panel, click the Start/Stop button of Apache module to start and stop the Apache web server process. Click the Start/Stop button of MySQL module to start and stop the MySQL DB server process.

![](/img/local-dev-install/image13.png)
  
When the module has been started successfully, the status color will turn green, the process ID (PID) is shown, and the port numbers used by the module will also be shown (80, 443, 3306).

![](/img/local-dev-install/image3.png)
  
#### Verify that Apache web server Module working state

Open your web browser, type, and access the following URL:

`http://localhost`

If you see the XAMPP dashboard page as something like in the following screen, then Apache web server is running and serving your local web files located on:

`C:\xampp\htdocs`

Therefore, any files located on the web server's document root directory `C:\xampp\htdocs` can be accessed from URL: 

`http://localhost` 

![](/img/local-dev-install/image31.png)

:::note
With XAMPP, MariaDB server is installed instead of the actual MySQL server. However, MariaDB is an open source variant of MySQL server. Both DB servers have the same database connection interface. Therefore, most of MySQL program code can be run on MariaDB and vice versa. If you prefer installing the actual MySQL DB server, please refer to the Download and Install MySQL 5.7 Database Server section.
:::  

If you get an error message when accessing the localhost using a web browser. Please refer to the Troubleshooting page.

### Database Alternative: MySQL Server 5.7

#### Downloading and Installing MySQL 5.7 Database Server

Open the following URL to access the download page of MySQL installer:

https://dev.mysql.com/downloads/windows/installer/5.7.html

Click the Download button that has a large file size of the installer (503.8 MB). This is the offline installer. Internet connection is not needed during installation. This installer is 32-bit version but also includes the 64-bit of MySQL server.

![](/img/local-dev-install/image29.png)


Click the following link: "No thanks, just start my download." to begin the download.

![](/img/local-dev-install/image32.png)

  
### Sourcetree Git Client
#### Downloading and Installing Sourcetree Git Client
Use a web browser to access and download Sourcetree, a free Git client application from the following URL:

https://sourcetreeapp.com

![](/img/local-dev-install/image15.png)

Download the Windows version of the Sourcetree application installer by clicking the Download for Windows button. The download will start immediately.
  

Run the downloaded installer file and in the Installation screen, if you already have a BitBucket account, you may connect and register the application with your BitBucket account. But, you may Skip creating an account for now. In this case, let's skip the BitBucket account registration by clicking the Skip button.

![](/img/local-dev-install/image18.png)
  
At the next page, Uncheck (untick) the Mercurial tool. The only tool needed to clone the KB system source code is Git, it is already selected, and we do not need Mercurial to be installed.

![](/img/local-dev-install/image44.png)
  
Click Next and enter your Name and Email address at the designated field. This information will be used to identify the author on every commit you made. The email address can also be used to create a free BitBucket account later.

![](/img/local-dev-install/image42.png)
  
Click the Next button and in the appearing Load SSH Key dialog click No button. You can configure SSH Key later for more convenience in committing and pushing the modified source code with Sourcetree.

![](/img/local-dev-install/image16.png)
  
The installation will begin and once Sourcetree has been installed, it will automatically start. If it does not start automatically after the installation ends, you can start Sourcetree from the shortcut on the Desktop or Start Menu.

## CLONE SOURCE CODE

### Kit-Build

Cloning KB System Source Code Using Sourcetree Git Client
Start the Sourcetree application from the Desktop or Start Menu shortcut if it has not been started. At the initial state of Sourcetree, you will get an empty repository tab like the following screen:

![](/img/local-dev-install/image19.png)
  
Click the Clone button from the application Toolbar. Enter the following information in the Clone screen:

| | |
|-|-|
| Repository URL      | [https://aryoxp@bitbucket.org/aryoxp/collabkb.git](https://aryoxp@bitbucket.org/aryoxp/collabkb.git) |
Destination Path      | `C:\xampp\htdocs\kb`
Name                  | `kb`
Checkout branch       | `kb` (Advanced Options)

![](/img/local-dev-install/image43.png)

Click the Clone button to start cloning (downloading) the KB system source code into the kb directory in the Apache web server document root folder (`C:\xampp\htdocs\kb`).
  
Sourcetree will begin cloning the KB system source code once you click the Clone button.

![](/img/local-dev-install/image45.png)
  
The cloning process will take some time. After the cloning process has been finished, you will see something like in the following screenshot:

![](/img/local-dev-install/image1.png)
  
For the information, the current working branch is: kb.
However, for the KB system to work, it needs a backend PHP framework code, namely `core-framework`, which will be explained on how to download and install the framework on the following section.

### Core-Framework

To clone and download the PHP framework from the Sourcetree application, click the Plus icon on the right of the kb tab:

![](/img/local-dev-install/image6.png)
  
Click the Clone button at the new Repository tab and enter the following information:

| | |
|-|-|
Repository URL        | [https://aryoxp@bitbucket.org/aryoxp/corefws.git](https://aryoxp@bitbucket.org/aryoxp/corefws.git)
Destination Path      | `C:\xampp\htdocs\corefws`
Name                  | `corefws`
Checkout branch       | `master` (open the Advanced Options section)
  
![](/img/local-dev-install/image35.png)

Click the Clone button to start cloning and downloading the PHP framework.
Once the cloning process has been finished, open a new Windows Explorer window and open the Apache document root directory at `C:\xampp\htdocs`.
  
![](/img/local-dev-install/image28.png)

Notice that there are **two** folders, i.e., `corefws` and `kb`, where the PHP framework and the KB system source code have been cloned inside the web server document root directory, respectively. 
Open the `corefws` directory, select and copy the following files and directory:
   * core-framework
   * favicon.ico
   * Index.php
  
![](/img/local-dev-install/image11.png)

Go one directory level up, back to the `htdocs` directory. 
Open the `kb` directory, and paste the copied files and directory inside the kb directory. 

The files and directory structure inside the kb directory should become something like in the following screenshot:
  
![](/img/local-dev-install/image34.png)

:::note
The main source code of the KB system is located inside the app directory. Most of the time of the development and modification of the KB system, the PHP code inside the core-framework directory should not need to be changed.
The basic architecture behind the KB system and Core-Framework is that the KB system is a "web app" in which run and served by a PHP framework called Core-Framework. The Core-Framework is a self-made MVC-based PHP framework that works similarly like other PHP frameworks, e.g. CodeIgniter, Laravel, Symfony, etc, to make server-side web application development simple and easy.
:::

When a user accesses the KB system application using, all HTML and Javascript codes, including images and other web-accessible files are served or generated by the framework from the app PHP code.

## CONFIGURATION

### Initializing Kit-Build System Database Structure and Data

The KB system source includes SQL files that are useful to create the database structure and initialize the necessary data. These SQL files are located in the `res` directory of `kb`. If the KB system location is in `C:\xampp\htdocs\kb`, then the SQL files are located in `C:\xampp\htdocs\kb\res` . 

The following SQL files and its purposes is as follows:
   * `kb-structure.sql` : initialize the KB system database structure.
   * `kb-structure-data.sql` : initialize the KB system database structure and prefill initial data.

In this example, the `kb-structure-data.sql` file will be used to automatically create the data structure and prefill the necessary initial user data. To execute the SQL file, a MySQL client program is required, however, phpMyAdmin (which should already be installed) can also be used to do the job.  

#### Initializing data structure using PhpMyAdmin

The step to initialize the data structure is as follows:
Use a web browser and open the following URL:

[http://localhost/phpmyadmin/](http://localhost/phpmyadmin/)

Click the New link at the left sidebar. A "Create New Database" page will be displayed on the right panel.

![](/img/local-dev-install/image5.png)

Enter the database name, this can be any name you want, but take a note of the database name because you will need the database name to configure the KB system. 

In this example, the database name is: `kb-dev`.

Ensure that the database collation is set to `utf8mb4_general_ci` to support multi language characters and international support, including Japanese kanji.
  
![](/img/local-dev-install/image30.png)

Once the database name and the collation type have been set. Click the Create button. The newly created `kb-dev` database will be shown in the database list on the sidebar and will be automatically selected.  

Click the `kb-dev` database on the sidebar to ensure that we are working on the `kb-dev` database and not on the other database.

![](/img/local-dev-install/image24.png)

On the Database panel toolbar, click the Import button to import the SQL file to initialize the database data structure.

![](/img/local-dev-install/image7.png)
   
In the Import tab page, click the Browse file button and locate the `kb-structure-data.sql` at `C:\xampp\htdocs\kb\res` directory.
  
![](/img/local-dev-install/image9.png)

Scroll down to the bottom of the page, and click the [**Go**] button to begin importing the SQL file.

![](/img/local-dev-install/image4.png)

A message similar to:

`Import has been successfully finished, 95 queries executed. (kb-structure-data.sql)`

will be shown when the database structure initialization has been successfully executed and all the tables of the KB system will be shown on the left sidebar.

### System Configuration

Before the KB system can be used and developed, It is necessary to configure the KB system database configuration to point to the newly created database.

Open Windows Explorer and open the following directory:
`C:\xampp\htdocs\kb\app\config`

And edit the `db.json` file using Notepad or other text editor application.

![](/img/local-dev-install/image22.png)

Change the database entry value from `kb` to your previously created database name.
In this example, the database name is `kb-dev` and change the contents like the following example:

```jsx {9} title="C:\xampp\htdocs\kb\app\config\db.json"
{
 "db": [{
   "key": "kb",
   "driver": "mysqli",
   "config": {
     "host": "127.0.0.1",
     "user": "root",
     "password": "",
      "database": "kb-dev",
      "charset": "utf8mb4",
     "collate": "utf8mb4_general_ci",
     "persistent": false
   }
 }]
}  
````

Save the `db.json` configuration file, and the local KB system is ready to be used. 

You should now be able to access the KB system using a web browser with the following URL:

[http://localhost/kb](http://localhost/kb)

:::success
Congratulations, you have successfully installed and configured a new KB system!
:::

---

- KB System Home Page:
  [http://localhost/kb](http://localhost/kb)
- System Administration/Management Page: [http://localhost/kb/index.php/home/admin](http://localhost/kb/index.php/home/admin) 
- Analyzer Page: 
[http://localhost/kb/index.php/home/analyzer](http://localhost/kb/index.php/home/analyzer)