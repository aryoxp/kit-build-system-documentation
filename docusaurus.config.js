/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'Kit-Build',
  tagline: 'Kit-Build concept maps are cool',
  url: 'https://your-docusaurus-test-site.com',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'facebook', // Usually your GitHub org/user name.
  projectName: 'docusaurus', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'Kit-Build',
      logo: {
        alt: 'My Site Logo',
        src: 'img/logo.svg',
      },
      items: [
        {
          to: '/tutorial/index',
          // docId: 'tutorial',
          label: 'Tutorial', 
          position: 'left'
        },
        {
          to: '/development/dev-setup',
          // docId: 'tutorial',
          label: 'Development', 
          position: 'left'
        },
        {
          type: 'doc',
          docId: 'intro',
          position: 'left',
          label: 'Kit-Build API',
        },
        // {to: '/blog', label: 'Blog', position: 'left'},
        {
          to: '/core/index',
          // docId: 'core',
          label: 'Core Framework API', 
          position: 'left'
        },
        // {
        //   to: '/product/halo',
        //   docId: 'product',
        //   label: 'Product', 
        //   position: 'left'
        // },
        // {
        //   href: 'https://github.com/facebook/docusaurus',
        //   label: 'GitHub',
        //   position: 'right',
        // },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Docs',
          items: [
            {
              label: 'Tutorial',
              to: '/tutorial/index',
            },
            {
              label: 'Kit-Build API',
              to: '/docs/intro',
            },
            {
              label: 'Core Framework API',
              to: '/core/index',
            },
          ],
        },
        {
          title: 'Community',
          items: [
            {
              label: 'Stack Overflow',
              href: 'https://stackoverflow.com/questions/tagged/docusaurus',
            },
            {
              label: 'Discord',
              href: 'https://discordapp.com/invite/docusaurus',
            },
            {
              label: 'Twitter',
              href: 'https://twitter.com/docusaurus',
            },
          ],
        },
        {
          title: 'More',
          items: [
            {
              label: 'Blog',
              to: '/blog',
            },
            {
              label: 'GitHub',
              href: 'https://github.com/facebook/docusaurus',
            },
          ],
        },
      ],
      copyright: `<span style="font-size: smaller; display:block; padding-top:3em;">Copyright © 2018-${new Date().getFullYear()} Learning Engineering Laboratory, Hiroshima University.<br>Built with Facebook/Docusaurus. Made with 😊 in Japan.</span>`,
    },
    prism: {
      usePrism: ['jsx'],
      additionalLanguages: ['php', 'markup'],
      theme: require('prism-react-renderer/themes/nightOwlLight'),
      darkTheme: require('prism-react-renderer/themes/oceanicNext'),
    },
    // colorMode: {
    //   // "light" | "dark"
    //   defaultMode: 'light',

    //   // Hides the switch in the navbar
    //   // Useful if you want to support a single color mode
    //   disableSwitch: false,

    //   // Should we use the prefers-color-scheme media-query,
    //   // using user system preferences, instead of the hardcoded defaultMode
    //   respectPrefersColorScheme: false,

    //   // Dark/light switch icon options
    //   switchConfig: {
    //     // Icon for the switch while in dark mode
    //     darkIcon: 'D',// '🌙',

    //     // CSS to apply to dark icon,
    //     // React inline style object
    //     // see https://reactjs.org/docs/dom-elements.html#style
    //     darkIconStyle: {
    //       marginLeft: '2px',
    //     },

    //     // Unicode icons such as '\u2600' will work
    //     // Unicode with 5 chars require brackets: '\u{1F602}'
    //     lightIcon: 'L',// '\u{1F602}',

    //     lightIconStyle: {
    //       marginLeft: '1px',
    //     },
    //   },
    // },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl:
            'https://github.com/facebook/docusaurus/edit/master/website/',
        },
        blog: {
          showReadingTime: true,
          // routeBasePath: '/',
          // Please change this to your repo.
          editUrl:
            'https://github.com/facebook/docusaurus/edit/master/website/blog/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
  plugins: [
    [
      '@docusaurus/plugin-content-docs',
      {
        id: 'core', // omitted => default instance
        path: 'core',
        routeBasePath: 'core',
        sidebarPath: require.resolve('./sidebars.js'),
        // ... other options
      },
    ],
    [
      '@docusaurus/plugin-content-docs',
      {
        id: 'tutorial', // omitted => default instance
        path: 'tutorial',
        routeBasePath: 'tutorial',
        sidebarPath: require.resolve('./sidebars.js'),
        // ... other options
      },
    ],
    [
      '@docusaurus/plugin-content-docs',
      {
        id: 'development', // omitted => default instance
        path: 'development',
        routeBasePath: 'development',
        sidebarPath: require.resolve('./sidebars.js'),
        // ... other options
      },
    ],
  ],
};
